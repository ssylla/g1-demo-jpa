package ihm;

import domain.bank.Adresse;
import domain.bank.Banque;
import domain.bank.Client;
import domain.bank.Compte;
import domain.book.Address;
import domain.book.Skill;
import domain.book.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App {
	
	public static void main( String[] args ) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "tp-4" );
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Client client = em.find( Client.class, 1L );
		System.out.println("=======");
		System.out.println(client);
		System.out.println("=======");
		// Banque segabank = new Banque("Sega Bank");
		// Client client = new Client();
		// client.setPrenom( "Séga" );
		// client.setGenre(Client.Genre.M);
		// client.setAdresse( new Adresse( 2, "rue de la soif", 44000, "NTE" ) );
		// client.setBanque( segabank );
		// Client client2 = new Client();
		// client2.setPrenom( "Séga 2" );
		// client2.setGenre(Client.Genre.M);
		// client2.setAdresse( new Adresse( 2, "rue de la soif", 44000, "NTE" ) );
		// client2.setBanque( segabank );
		// em.persist( segabank );
		//
		em.getTransaction().commit();
		em.close();
		
		emf.close();
		// createUser();
		// dspUserSkills();
	}
	
	private static void dspUserSkills() {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "demo-jpa" );
		EntityManager em = emf.createEntityManager();
		// User user = em.find( User.class, 1 );
		// System.out.println( user );
		// for ( Skill item : user.getSkills() ) {
		// 	System.out.println( item );
		// }
		Skill java = em.find( Skill.class, 1 );
		em.close();
		emf.close();
		System.out.println(java.getUser());
	}
	
	private static void createUser() {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "demo-jpa" );
		EntityManager em = emf.createEntityManager();
		// EntityTransaction transaction = em.getTransaction();
		em.getTransaction().begin();
		// User user = em.find( User.class, 1 );
		// System.out.println(user);
		
		
		// User user = new User( "Séga S.", "ssylla@ss.org" );
		// Skill skill = new Skill("Java", user);
		// Skill php = new Skill("PHP", user);
		
		User sega = new User( "Séga S.", "ssylla@ss.org" );
		sega.setAddress(new Address(2, "Rue de la soif", "44000", "NTE") );
		sega.addSkill( new Skill( "Java" ) );
		sega.addSkill( new Skill( "PHP" ) );
		em.persist( sega );
		em.getTransaction().commit();
		em.close();
		emf.close();
	}
}
