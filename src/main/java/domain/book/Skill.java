package domain.book;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Skill implements Serializable {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id;
	private String label;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE}/*, fetch = FetchType.LAZY */)
	@JoinColumn( name = "user_id" )
	private User user;
	
	public Skill() {}
	
	public Skill( String label ) {
		this.label = label;
	}
	
	public Skill( String label, User user ) {
		this.label = label;
		this.user = user;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getLabel() {
		return label;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser( User user ) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Skill{" );
		sb.append( "id=" ).append( id );
		sb.append( ", label='" ).append( label ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
	
	
}