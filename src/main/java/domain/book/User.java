package domain.book;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User implements Serializable {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id;
	private String name;
	private String email;
	private String phone;
	@Embedded
	private Address address;
	
	@OneToMany( mappedBy = "user", cascade = CascadeType.PERSIST/*, fetch = FetchType.EAGER*/ )
	private Set<Skill> skills = new HashSet<>();
	
	
	public User() {}
	
	public User( String name, String email ) {
		this.name = name;
		this.email = email;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public Set<Skill> getSkills() {
		return skills;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress( Address address ) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "User{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
	
	public void addSkill( Skill skill ) {
		skills.add( skill );
		skill.setUser( this );
	}
}
