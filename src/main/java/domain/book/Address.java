package domain.book;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Address implements Serializable {
	
	private int number;
	@Column(name = "NOM_DE_LA_VOIE", length = 2500)
	private String street;
	private String zipCode;
	@Column(columnDefinition = "TEXT")
	private String city;
	
	public Address() {}
	
	public Address( int number, String street, String zipCode, String city ) {
		this.number = number;
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
	}
	
	public int getNumber() {
		return number;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public String getCity() {
		return city;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Address{" );
		sb.append( "number=" ).append( number );
		sb.append( ", street='" ).append( street ).append( '\'' );
		sb.append( ", zipCode='" ).append( zipCode ).append( '\'' );
		sb.append( ", city='" ).append( city ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
