package domain.bank;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class Client implements Serializable {

	
	public enum Genre {
		M("Masculin"), F("Fe"), O("Autre");
		
		private String label;
		
		Genre() {
		}
		
		Genre( String label ) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
		
		public void setLabel( String label ) {
			this.label = label;
		}
	}
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String prenom;
	
	@Enumerated(EnumType.STRING)
	private Genre genre;
	private LocalDate dateNaissance;
	@Embedded
	private Adresse adresse;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "banque_id")
	private Banque banque;
	
	public Client() {}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom( String prenom ) {
		this.prenom = prenom;
	}
	
	public Genre getGenre() {
		return genre;
	}
	
	public void setGenre( Genre genre ) {
		this.genre = genre;
	}
	
	public LocalDate getDateNaissance() {
		return dateNaissance;
	}
	
	public void setDateNaissance( LocalDate dateNaissance ) {
		this.dateNaissance = dateNaissance;
	}
	
	public Adresse getAdresse() {
		return adresse;
	}
	
	public void setAdresse( Adresse adresse ) {
		this.adresse = adresse;
	}
	
	public Banque getBanque() {
		return banque;
	}
	
	public void setBanque( Banque banque ) {
		if( null != this.banque ) {
			this.banque.getClients().remove( this );
		}
		this.banque = banque;
		if( null != this.banque ) {
			this.banque.getClients().add( this );
		}
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Client{" );
		sb.append( "id=" ).append( id );
		sb.append( ", prenom='" ).append( prenom ).append( '\'' );
		sb.append( ", genre=" ).append( genre );
		sb.append( ", dateNaissance=" ).append( dateNaissance );
		sb.append( ", adresse=" ).append( adresse );
		sb.append( ", banque=" ).append( banque );
		sb.append( '}' );
		return sb.toString();
	}
}
