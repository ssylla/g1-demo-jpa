package domain.bank;

import javax.persistence.Entity;

@Entity
public class Virement extends Operation{
	
	private String beneficiaire;
	
	public Virement() {
	}
	
	public String getBeneficiaire() {
		return beneficiaire;
	}
	
	public void setBeneficiaire( String beneficiaire ) {
		this.beneficiaire = beneficiaire;
	}
}
