package domain.bank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// @DiscriminatorColumn(name = "com_type")
public class Compte implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String num;
	private double solde;
	
	@ManyToMany
	@JoinTable(
			name = "cli_com",
			joinColumns = @JoinColumn(name= "com_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "cli_id", referencedColumnName = "id")
	)
	private Set<Client> clients;
	@OneToMany(mappedBy = "compte")
	private Set<Operation> operations;
	
	public Compte() {}
	
	public String getNum() {
		return num;
	}
	
	public void setNum( String num ) {
		this.num = num;
	}
	
	public double getSolde() {
		return solde;
	}
	
	public void setSolde( double solde ) {
		this.solde = solde;
	}
	
	public Set<Client> getClients() {
		return clients;
	}
	
	public void setClients( Set<Client> clients ) {
		this.clients = clients;
	}
	
	public Set<Operation> getOperations() {
		return operations;
	}
	
	public void setOperations( Set<Operation> operations ) {
		this.operations = operations;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Compte{" );
		sb.append( "num='" ).append( num ).append( '\'' );
		sb.append( ", solde=" ).append( solde );
		sb.append( ", clients=" ).append( clients );
		sb.append( ", operations=" ).append( operations );
		sb.append( '}' );
		return sb.toString();
	}
}
