package domain.bank;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Adresse implements Serializable {
	
	@Column(nullable = false)
	private Integer num;
	@Column(nullable = false)
	private String rue;
	private int codePostal;
	private String ville;
	
	public Adresse() {
	}
	
	public Adresse( Integer num, String rue, int codePostal, String ville ) {
		this.num = num;
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
	}
	
	public int getNum() {
		return num;
	}
	
	public void setNum( int num ) {
		this.num = num;
	}
	
	public String getRue() {
		return rue;
	}
	
	public void setRue( String rue ) {
		this.rue = rue;
	}
	
	public int getCodePostal() {
		return codePostal;
	}
	
	public void setCodePostal( int codePostal ) {
		this.codePostal = codePostal;
	}
	
	public String getVille() {
		return ville;
	}
	
	public void setVille( String ville ) {
		this.ville = ville;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Adresse{" );
		sb.append( "num=" ).append( num );
		sb.append( ", rue='" ).append( rue ).append( '\'' );
		sb.append( ", codePostal=" ).append( codePostal );
		sb.append( ", ville='" ).append( ville ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
