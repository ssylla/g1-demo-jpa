package domain.bank;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.time.LocalDate;

@Entity
// @DiscriminatorValue( "AVIE" )
public class AssuranceVie extends Compte {
	
	private LocalDate dateFin;
	private double taux;
	
	public AssuranceVie() {
	}
	
	public LocalDate getDateFin() {
		return dateFin;
	}
	
	public void setDateFin( LocalDate dateFin ) {
		this.dateFin = dateFin;
	}
	
	public double getTaux() {
		return taux;
	}
	
	public void setTaux( double taux ) {
		this.taux = taux;
	}
}
