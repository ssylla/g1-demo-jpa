package domain.bank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Banque implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	
	@OneToMany(mappedBy = "banque", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Client> clients = new HashSet<>();
	
	public Banque() {
	}
	
	public Banque( String nom ) {
		this.nom = nom;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom( String nom ) {
		this.nom = nom;
	}
	
	public Set<Client> getClients() {
		return clients;
	}
	
	public void setClients( Set<Client> clients ) {
		this.clients = clients;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Banque{" );
		sb.append( "nom='" ).append( nom ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
	
	@PrePersist
	@PreUpdate
	public void updateClientAssociation() {
		// for(Client item : clients) {
		// 	item.setBanque( this );
		// }
	}
}
