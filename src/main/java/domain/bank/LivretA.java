package domain.bank;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
// @DiscriminatorValue( "LA" )
public class LivretA extends Compte {
	
	private double taux;
	
	public LivretA() { }
	
	public double getTaux() {
		return taux;
	}
	
	public void setTaux( double taux ) {
		this.taux = taux;
	}
}
